import java.util.List;
import method.Model;
import method.Parser;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.Arrays;

public class TestSolution {

    private Model model;

    private boolean equals(double[] ans) {
        boolean eq = true;
        try {
            List<Double> ret = model.runMethod();
            for (int i = 0; i < ret.size(); i++) {
                if (Math.abs(ans[i] - ret.get(i)) >= 1e-2) {
                    eq = false;
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return eq;
    }

    private void run(String[] bounds, String function) {
        System.out.println();
        System.out.println(function);
        model = new Model(Parser.parseFunction(function));
        int n = bounds.length;
        for (int i = 0; i < n; i++) {
            System.out.println(bounds[i]);
            model.addBound(Parser.parseBound(bounds[i]), i + 1);
        }
        model.initFirstTable();

        //model.printTable();
    }

    private void runWithAnswer(String[] bounds, String function, double[] ans) {
        run(bounds, function);
        Assertions.assertTrue(equals(ans));
    }

    private void runWithError(String[] bounds, String function) {
        run(bounds, function);
        System.out.println("Нет решения");
        Assertions.assertThrows(RuntimeException.class, () -> model.runMethod());
    }

    @Test
    public void first_min() {
        String[] bounds = new String[]{
                "1x1+1x2>=4",
                "2x1-1x2>=1",
                "1x1-2x2<=1"
        };
        runWithError(bounds, "f=2x1-3x2+1->min");
    }

    @Test
    public void first_max() {
        String[] bounds = new String[]{
                "1x1+1x2>=4",
                "2x1-1x2>=1",
                "1x1-2x2<=1"
        };
        runWithError(bounds, "f=2x1-3x2+1->max");
    }

    @Test
    public void second() {
        String[] bounds = new String[]{
                "1x1-1x2<=-1",
                "1x1-1x2>=-3",
                "1x1+0x2<=3"
        };

        double[] ans = new double[] {
                15.0, 3.0, 6.0
        };
        runWithAnswer(bounds, "f=1x1+2x2+0->max", ans);
    }

    @Test
    public void third() {
        String[] bounds = new String[]{
                "1x1+1x2=10",
                "1x1+0x2>=4"
        };

        double[] ans = new double[] {
                -24.0, 4.0, 6.0
        };

        runWithAnswer(bounds, "f=-3x1-2x2+0->max", ans);
    }

    @Test
    public void fourth() {
        String[] bounds = new String[]{
                "-4x1+1x2<=4",
                "1x1-2x2<=-6",
                "1x1+1x2<=6"
        };
        double[] ans = new double[] {
                -5.2, 0.4, 5.6
        };
        runWithAnswer(bounds, "f=1x1-1x2+0->min", ans);
    }

    @Test
    public void fifth() {
        String[] bounds = new String[]{
                "-2x1+1x2<=2",
                "1x1-1x2<=-8",
                "1x1+1x2<=5"
        };
        runWithError(bounds, "f=1x1-1x2+0->max");
    }

    @Test
    public void sixth() {
        String[] bounds = new String[]{
                "1x1+1x2+1x3+1x4<=15",
                "7x1+5x2+3x3+2x4<=120",
                "3x1+5x2+10x3+15x4<=100"
        };
        double[] ans = new double[] {
                99.28, 7.14, 0.0, 7.85, 0.0
        };
        runWithAnswer(bounds, "f=4x1+5x2+9x3+11x4+0->max", ans);
    }

    @Test
    public void seventh() {
        String[] bounds = new String[]{
                "-1x1+1x2<=2",
                "-1x1+3x2<=10",
                "5x1+1x2<=30"
        };
        double[] ans = new double[] {
                -40.0, 2.0, 4.0
        };
        runWithAnswer(bounds, "f=10x1-15x2+0->min", ans);
    }

    @Test
    public void eighth() {
        String[] bounds = new String[]{
                "1x1+0x2+0x3+1x4+1x5=2",
                "0x1+1x2+0x3-1x4+1x5=7",
                "0x1+0x2+1x3+1x4-1x5=17"
        };
        double[] ans = new double[] {
                -1.0, 2.0, 7.0, 17.0, 0.0, 0.0
        };
        runWithAnswer(bounds, "f=0x1+0x2+0x3-5x4-7x5-1->max", ans);
    }
}
