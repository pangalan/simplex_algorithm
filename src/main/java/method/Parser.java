package method;

import common.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Parser {

    public static List<Double> parseCoef(String s) {
        List<Double> result = new ArrayList<>();
        if (s.charAt(0) != '-')
            s = '+' + s;
        String[] parts = s.split("x[0-9]+");
        for (String x : parts) {
            Double num = Double.valueOf(x.substring(1));
            if (x.charAt(0) == '-')
                result.add(num * -1.0);
            else
                result.add(num);
        }
        return result;
    }

    public static Pair parseBound(String s) {
        String[] parts = s.split("<=|>=|<|>|=");
        Double available = Double.valueOf(parts[1].trim());
        Integer mode = available < 0 ? -1 : 1;
        if (available < 0) {
            available *= -1;
            if (s.contains(">"))
                s = s.replace('>', '<');
            else
                s = s.replace('<', '>');
        }
        boolean eq = false;
        if (!s.contains("<") && !s.contains(">"))
            eq = true;
        List<Double> result = parseCoef(parts[0].trim());
        result = result.stream().map(i -> i * mode).collect(Collectors.toList());
//        result.add(Double.valueOf(parts[1].trim()));
        if (s.contains(">"))
            result.addAll(new ArrayList<Double>() {{add(-1.0); add(1.0);}});
        else
            result.addAll(new ArrayList<Double>() {{add(1.0); add(1.0);}});
        return new Pair(result, new Pair(available, eq));
    }

    public static Pair parseFunction(String s) {
        s = s.substring(s.indexOf('=') + 1);
        String[] parts = s.split("->");
        Integer mode = parts[1].trim().compareTo("max") == 0 ? 1 : -1;
        List<Double> coefs = parseCoef(parts[0].trim());
        coefs.set(coefs.size() - 1, coefs.get(coefs.size() - 1));
        return new Pair(new Pair(coefs.stream().limit(coefs.size() - 1).map(i -> i * mode).collect(Collectors.toList()),
                coefs.get(coefs.size() - 1)), mode);
    }

}
