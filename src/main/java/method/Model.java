package method;

import common.Pair;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class Model {

    private List<Double> function;
    private List<List<Double>> bounds;
    private List<Pair<Integer, String>> available;
    private List<Pair<Integer, String>> basic;
    private List<Double> value;
    private List<Double> fakeVars;
    private List<Double> linear;
    private double[][] table;
    private Integer mode;
    private int countBasic;
    private int countAvailable;
    private int countX;
    private List<Integer> eqs;

    public Model(Pair<Pair<List<Double>, Double>, Integer> function) {
        this.function = function.first().first();
        this.countX = function.first().first().size();
        this.value = new ArrayList<Double>(){{add(function.first().second());}};
        this.bounds = new ArrayList<>();
        this.mode = function.second();
        this.fakeVars = new ArrayList<>();
        this.available = new ArrayList<>();
        this.basic = new ArrayList<>();
        this.eqs = new ArrayList<>();
    }

    public void addBound(Pair<List<Double>, Pair<Double, Boolean>> list, int ind) {
        bounds.add(list.first());
        if (list.second().second())
            eqs.add(ind);
        value.add(list.second().first());
    }

    public List<Double> runMethod() throws Exception {
        int iterations = 1;
        int line = countBasic + 1;
        while (true) {
            printTable();
            int index = line;
            Optional tmp = IntStream.range(0, countAvailable)
                    .boxed()
                    .filter(i -> (available.get(i).second().contains("x") && table[index][i] < 0
                            && !eqs.contains(available.get(i).first() - countX)))
                    .findFirst();
            if (!tmp.isPresent())
                break;
            int indexAvailable = (int)tmp.get();
            System.out.println("\nИтерация " + iterations++);
            double min = Double.MAX_VALUE;
            int changeAvailable = available.get(indexAvailable).first();
            int indexBasic = -1;
            for (int i = 1; i < countBasic + 1; i++) {
                if (table[i][indexAvailable] > 0) {
                    if (table[i][countAvailable] / table[i][indexAvailable] < min) {
                        min = table[i][countAvailable] / table[i][indexAvailable];
                        indexBasic = i;
                    }
                }
            }
            if (indexBasic == -1)
                throw new RuntimeException();
            int changeBasic = basic.get(indexBasic - 1).first();
            Pair<Integer, String> cur = basic.get(indexBasic - 1);
            basic.set(indexBasic - 1, available.get(indexAvailable));
            available.set(indexAvailable, cur);
            /*System.out.println("Выполнение процедуры: " + available.get(indexAvailable).second()
                    + available.get(indexAvailable).first() + " <-> " + basic.get(indexBasic - 1).second()
                    + basic.get(indexBasic - 1).first());*/
            List<Double> prev = Arrays.stream(table[indexBasic]).boxed().collect(Collectors.toList());
            double term = table[indexBasic][indexAvailable];
            for (int i = 0; i < countAvailable + 1; i++)
                table[indexBasic][i] /= term;
            table[indexBasic][indexAvailable] = 1 / term;
            for (int i = 0; i < countBasic + 2; i++) {
                if (i == indexBasic)
                    continue;
                table[i][indexAvailable] /= term * -1;
            }
            for (int i = 0; i < table.length; i++) {
                for (int j = 0; j < table[i].length; j++) {
                    if (i == indexBasic || j == indexAvailable)
                        continue;
                    table[i][j] += prev.get(j) * table[i][indexAvailable];
                }
            }
            if (line != 0) {
                boolean exist = false;
                for (double d : table[countBasic + 1]) {
                    if (d < 0) {
                        exist = true;
                        break;
                    }
                }
                if (!exist)
                    line = 0;
            }
        }
        for (Pair<Integer, String> pair : basic) {
            if (pair.second().contains("y"))
                throw new RuntimeException();
        }
        printTable();

        return printAnswer();
    }

    public void initFirstTable() {
        countAvailable = countX + bounds.size();
        countBasic = bounds.size();
        table = new double[bounds.size() + 2][countAvailable + 1];
        for (int i = 0; i < countX; i++)
            table[0][i] = function.get(i) * -1;
        table[0][countAvailable] = value.get(0);
        for (int i = 0; i < bounds.size(); i++) {
            for (int j = 0; j < countX; j++) {
                table[i + 1][j] = bounds.get(i).get(j);
            }
            table[i + 1][countX + i] = bounds.get(i).get(countX);
            table[i + 1][countAvailable] = value.get(i + 1);
        }
        for (int i = 0; i < countAvailable + 1; i++) {
            for (int j = 0; j < bounds.size(); j++)
                table[countBasic + 1][i] -= table[j + 1][i];
        }
        for (int i = 0; i < countAvailable; i++)
            available.add(new Pair<>(i + 1, "x"));
        for (int j = 0; j < countBasic; j++)
            basic.add(new Pair<>(j + 1, "y"));
//        available = IntStream.range(1, countAvailable + 1).boxed().collect(Collectors.toList());
//        basic = IntStream.range(countAvailable + 1, countAvailable + countBasic + 1).boxed().collect(Collectors.toList());
    }

    public List<Double> printAnswer() {
        System.out.println("Оптимальное решение:");
        System.out.format("f = %.2f\n",table[0][countAvailable] * mode);
        double[] ans = new double[countBasic + countAvailable];
        for (int i = 1; i <= countBasic; i++) {
            ans[basic.get(i - 1).first() - 1] = table[i][countAvailable];
        }
        for (int i = 0; i < countAvailable + countBasic; i++) {
            System.out.format("x%d = %.2f\n", (i + 1), ans[i]);
        }
        /*System.out.println("Базисные переменные:");
        for (int i = 1; i <= countBasic; i++) {
        }*/
        return new ArrayList<Double>() {
            {
                add(table[0][countAvailable] * mode);
                addAll(Arrays.stream(ans).limit(countX).boxed().collect(Collectors.toList()));
            }
        };
    }

    public void printTable() {
        System.out.format("%10s", " ");
        for (int i = 0; i < countAvailable; i++)
            System.out.format("%10s", available.get(i).second() + available.get(i).first());
        System.out.format("%10s\n", "св");
        System.out.format("%10s", "f");
        for (int i = 0; i < countAvailable + 1; i++) {
            System.out.format("%10.3f", table[0][i]);
        }
        System.out.println();
        for (int i = 0; i < countBasic; i++) {
            System.out.format("%10s", basic.get(i).second() + basic.get(i).first());
            for (int j = 0; j < countAvailable + 1; j++)
                System.out.format("%10.3f", table[i + 1][j]);
            System.out.println();
        }
        System.out.format("%10s", "t");
        for (int i = 0; i < countAvailable + 1; i++) {
            System.out.format("%10.3f", table[countBasic + 1][i]);
        }
        System.out.println();
    }

}
