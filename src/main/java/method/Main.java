package method;

import common.Tokenizer;

import java.io.File;
import java.io.FileReader;

public class Main {

    public static void main(String[] args) throws Exception {
        Tokenizer input = new Tokenizer(new FileReader("input.txt"));

        String function = input.next();

        int n = input.nextInt();

        Model model = new Model(Parser.parseFunction(function));
        for (int i = 0; i < n; i++){
            model.addBound(Parser.parseBound(input.next()), i + 1);
        }

        model.initFirstTable();

//        model.printTable();

        try {
            model.runMethod();
        } catch(Exception e) {
            System.out.println("Нет решения");
        }

    }

}
